﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{

    public class EntityManager : MonoBehaviour
    {
        public Entity[] entities;
        public void Log(object log)
        {
            Debug.Log(log);

        }
        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Names are being registered");
            for (int i = 0; i < entities.Length; i++)
            {
                Debug.Log("Index: " + i + ", Entity Name: " + entities[i].EntityName);
            }
        }
    }
}

