﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{
    interface IAttack
    {
        void AttackEnemy(Enemy enemy, int damageAmt);
    }
}