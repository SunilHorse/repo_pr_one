﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{

    public class Entity : MonoBehaviour
    {
        [SerializeField]
        private string entityName;
        public string EntityName
        {
            get { return entityName; }
            set { entityName = value; }
        }
        public void Log(object log)
        {
            Debug.Log(log);
        }
    }
}