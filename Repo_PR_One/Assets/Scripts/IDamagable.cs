﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{
    interface IDamagable
    {
        void Damage(int damageAmt);
    }
}