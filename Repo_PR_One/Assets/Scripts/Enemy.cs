﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        private float currentHealth;
        [SerializeField]
        private int attackAmount;
        [SerializeField]
        private bool isAlive;
        public void Damage(int damageAmt)
        {
            if (currentHealth > 0)
            {
                currentHealth = currentHealth - damageAmt;
                if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Debug.Log(EntityName + " has a health of " + currentHealth);
                    return;
                }
            }
            else
            {
                currentHealth = 0;
                isAlive = false;
                Debug.Log(EntityName + " has a health of " + currentHealth);
                return;
            }
            Debug.Log(EntityName + " has a health of " + currentHealth);
        }
        public bool IsAlive
        {
            get { return isAlive; }
            set { IsAlive = value; }
        }
    }
}

