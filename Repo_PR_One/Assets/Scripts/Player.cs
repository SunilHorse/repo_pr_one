﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PROne
{

    public class Player : Entity, IAttack
    {
        [SerializeField]
        private float currentHealth;
        [SerializeField]
        private int attackAmount;
        [SerializeField]
        private bool isAlive;
        [SerializeField]
        private Enemy enemyEntity;

        public void AttackEnemy(Enemy enemyEntity, int attackAmount)
        {
            if (enemyEntity.IsAlive == true)
            {
                enemyEntity.Damage(attackAmount);
            }
        }
        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Player Name: " + EntityName);
            Debug.Log("Player Health: " + currentHealth);
            Debug.Log("Player Attack Amount: " + attackAmount);
            AttackEnemy(enemyEntity, attackAmount);
        }
    }
}
